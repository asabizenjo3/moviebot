# app.py
import os

from flask import Flask, render_template, request, jsonify, redirect, url_for
import requests
from transformers import BertTokenizer, BertModel
import torch
import numpy as np
from datetime import datetime

app = Flask(__name__)

# Replace with your TMDb API key
API_KEY = os.getenv('TMDB_API_KEY')
BASE_URL = 'https://api.themoviedb.org/3'

def get_movie_id(movie_title):
    search_url = f'{BASE_URL}/search/movie?api_key={API_KEY}&query={movie_title}'
    response = requests.get(search_url)
    data = response.json()
    if data['results']:
        return data['results'][0]['id']
    else:
        return None

def get_movie_details(movie_id):
    movie_url = f'{BASE_URL}/movie/{movie_id}?api_key={API_KEY}'
    response = requests.get(movie_url)
    return response.json()

def get_movies_by_genres(genre_ids, original_release_year, exclude_movie_id, rating_threshold=7.0, max_results=100):
    start_year = max(original_release_year - 5, 1900)
    end_year = datetime.now().year

    movies = []
    page = 1
    while len(movies) < max_results:
        discover_url = f'{BASE_URL}/discover/movie?api_key={API_KEY}&with_genres={",".join(map(str, genre_ids))}&primary_release_date.gte={start_year}-01-01&primary_release_date.lte={end_year}-12-31&page={page}&vote_average.gte={rating_threshold}'
        response = requests.get(discover_url)
        data = response.json()
        if 'results' in data:
            for movie in data['results']:
                if movie['id'] != exclude_movie_id:
                    # Extend movie details with ranking information
                    movie_details = get_movie_details(movie['id'])
                    movie['ranking'] = movie_details['vote_average']  # Add ranking based on vote_average
                    movies.append(movie)
            if page >= data['total_pages']:
                break
            page += 1
        else:
            break

    return movies[:max_results]

def compute_bert_similarity(original_plot, plots, batch_size=8):
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    model = BertModel.from_pretrained('bert-base-uncased')

    similarities = []
    for i in range(0, len(plots), batch_size):
        batch_plots = plots[i:i+batch_size]
        tokenized = tokenizer([original_plot] + batch_plots, return_tensors='pt', padding=True, truncation=True)

        with torch.no_grad():
            outputs = model(**tokenized)
            embeddings = outputs.last_hidden_state[:, 0, :].numpy()

        original_embedding = embeddings[0]
        for embedding in embeddings[1:]:
            similarity = np.dot(original_embedding, embedding) / (np.linalg.norm(original_embedding) * np.linalg.norm(embedding))
            similarities.append(similarity.item())

    return similarities

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        movie_title = request.form['movie_title']
        print(f"Received movie title: {movie_title}")

        movie_id = get_movie_id(movie_title)

        if movie_id:
            movie_details = get_movie_details(movie_id)
            original_plot = movie_details['overview']
            original_release_year = int(movie_details['release_date'][:4])
            genre_ids = [genre['id'] for genre in movie_details.get('genres', [])]

            similar_movies = get_movies_by_genres(genre_ids, original_release_year, movie_id)

            if similar_movies:
                plots = [movie['overview'] for movie in similar_movies]

                # Assuming compute_bert_similarity function computes similarity scores
                similarities = compute_bert_similarity(original_plot, plots)

                movie_similarity_pairs = list(zip(similar_movies, similarities))
                movie_similarity_pairs.sort(key=lambda x: x[1], reverse=True)

                # Prepare data to send back to the frontend
                result_data = {
                    'movie_title': movie_title,
                    'movie_details': movie_details,
                    'similar_movies': movie_similarity_pairs[:10]
                }

                # Return JSON response to handle in AJAX success function
                return jsonify(result_data)

            else:
                error_message = "No similar movies found."
                return jsonify({'error': error_message})

        else:
            error_message = "Movie not found."
            return jsonify({'error': error_message})

    return render_template('index.html')

if __name__ == "__main__":
    app.run(debug=True)
